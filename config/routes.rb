Spree::Core::Engine.routes.draw do

  resources :contacts, controller: 'contact_us/contacts'
  get '/contact-us' => 'contact_us/contacts#index', :as => :contact_us
end
